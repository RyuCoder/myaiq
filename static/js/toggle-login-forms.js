
$(document).ready(function () {

    $("div.toggle div.btn-group").find('label').on("click", function () {
        $this = $(this);
        $user_type = $('input#id_user_type');
        $user_type_label = $('label[for="id_username"]');

        console.log("\n");
        console.log($user_type.val());
        console.log();
        console.log("\n");
        
        if (!$this.hasClass("active")) {

            if ($this.children('input').attr("name") == 'telegram') {
                $this.prev().removeClass("btn-primary");
                $this.prev().addClass("btn-secondary");
                $this.removeClass("btn-secondary");
                $this.addClass("btn-primary");
                
                $user_type.val('telegram')
                $user_type_label.text('Telegram ID')
                
            } else {
                $this.next().removeClass("btn-primary");
                $this.next().addClass("btn-secondary");
                $this.removeClass("btn-secondary");
                $this.addClass("btn-primary");
                
                $user_type.val('twitter')
                $user_type_label.text('Twitter ID')
            }

        }

    });

});