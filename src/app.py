from flask import Flask


app = Flask(__name__)
 
# Views need to be imported after the app variable is defined
from views import *


if __name__ == "__main__":
    app.run(debug=True)