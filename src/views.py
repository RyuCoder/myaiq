from flask import render_template

from app import app 


@app.route('/')
@app.route('/home')
def index_get():
    return render_template('index.html')

